package id.app.wawetech.massagemitra

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import id.app.wawetech.massagemitra.adapter.RiwayatAdapter

class RiwayatTransaksiActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acitivity_history_transaksi)
        title = "Riwayat Transaksi"
        var list = findViewById<RecyclerView>(R.id.list_transaksi)
        list.layoutManager = LinearLayoutManager(applicationContext)
        list.adapter = RiwayatAdapter(generateData())
    }


    fun generateData() : MutableList<String>{
        var data : MutableList<String> = ArrayList()
        for(i in 0 until 10){
            if(i % 2 == 0){
                data.add("Penambahan")
            }else{
                data.add("Pengurangan")
            }
        }
        return data
    }
}