package id.app.wawetech.massagemitra.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.app.wawetech.massagemitra.R

class AktifAdapter(var list: MutableList<Any>, var item: itemListener) : RecyclerView.Adapter<AktifAdapter.Holder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Holder {
        return Holder(LayoutInflater.from(p0.context).inflate(R.layout.row_pesanan_baru, p0, false), item)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: Holder, p1: Int) {
        p0.bindData()
    }


    interface itemListener {
        fun openItem()
    }

    class Holder(var itemview: View, var item: itemListener) : RecyclerView.ViewHolder(itemview) {

        fun bindData() {
            itemview.setOnClickListener {
                item.openItem()
            }
        }

    }
}