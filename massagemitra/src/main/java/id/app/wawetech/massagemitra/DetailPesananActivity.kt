package id.app.wawetech.massagemitra

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.RelativeLayout
import android.widget.TextView
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.*

class DetailPesananActivity : AppCompatActivity() {

    lateinit var locationPicker: RelativeLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_pesanan)
        title = "Detail Pesanan"

        var locationPicker = findViewById<RelativeLayout>(R.id.locationpicker)
        locationPicker.setOnClickListener {
            startActivity(Intent(this@DetailPesananActivity, MapsActivity::class.java))
        }
    }
}

