package id.app.wawetech.massagemitra

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.MenuItem
import android.widget.RelativeLayout
import id.app.wawetech.massagemitra.adapter.ViewPagerAdapter
import id.app.wawetech.massagemitra.fragment.*

class HomeActivity : AppCompatActivity() {
    var viewpager: ViewPager? = null
    val Beranda = 0
    val Pesanan = 1
    val Bantuan = 2
    val Akun = 3
    var prevMenuItem: MenuItem? = null
    lateinit var bottom_navigation : BottomNavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        findViewById<RelativeLayout>(R.id.head).setOnClickListener {
            startActivity(Intent(this@HomeActivity,ProfileActivity::class.java))
        }
        viewpager = findViewById<ViewPager>(R.id.viewpager)
        bottom_navigation = findViewById<BottomNavigationView>(R.id.bottom_navigation)
            bottom_navigation.setOnNavigationItemSelectedListener { it ->
            when (it.itemId) {
                R.id.action_beranda -> {
                    Log.e("", "pos" + it.itemId)
                    viewpager?.currentItem = Beranda
                }
                R.id.action_pesanan -> {
                    Log.e("", "pos" + it.itemId)
                    viewpager?.currentItem = Pesanan
                }
                R.id.action_bantuan -> {
                    Log.e("", "pos" + it.itemId)
                    viewpager?.currentItem = Bantuan
                }
                R.id.action_akun -> {
                    Log.e("", "pos" + it.itemId)
                    viewpager?.currentItem = Akun
                }
            }
            return@setOnNavigationItemSelectedListener true
        }

        viewpager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(p0: Int) {
                Log.e("", "posselected" + p0)

                if (prevMenuItem != null) {
                    prevMenuItem?.isChecked = false
                } else {
                    bottom_navigation.menu.getItem(0).isChecked = true
                }
                Log.e("HomeActivity", "position" + p0)
                bottom_navigation.menu.getItem(p0).isChecked = true
                prevMenuItem = bottom_navigation.menu.getItem(p0)
            }
        })
        setupViewpager(this!!.viewpager!!)
    }

    fun setupViewpager(viewPager: ViewPager) {
        Log.e("", "setup viewpager")
        val adapter: ViewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(Beranda())
        adapter.addFragment(Pesanan())
        adapter.addFragment(Statistik())
        adapter.addFragment(Saldo())
        viewpager?.adapter = adapter
    }
}
