package id.app.wawetech.massagemitra.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.app.wawetech.massagemitra.R


class UlasanAdapter(var list: MutableList<Any>) : RecyclerView.Adapter<UlasanAdapter.Holder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Holder {
        return Holder(LayoutInflater.from(p0.context).inflate(R.layout.row_ulasan, p0, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: Holder, p1: Int) {
    }

    class Holder(var view: View) : RecyclerView.ViewHolder(view) {

    }

}