package id.app.wawetech.massagemitra

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView

class BantuanActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = "Bantuan"
        setContentView(R.layout.bantuan_activity)
        findViewById<TextView>(R.id.txtKeluhan).setOnClickListener {
            startActivity(Intent(this@BantuanActivity,KeluhanActivity::class.java))
        }

        findViewById<TextView>(R.id.txtHubungiKami).setOnClickListener {

        }
    }
}