package id.app.wawetech.massagemitra.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import id.app.wawetech.massagemitra.R
import kotlinx.android.synthetic.main.saldo_frag.view.*
import id.app.wawetech.massagemitra.SetorUangActivity
import id.app.wawetech.massagemitra.TarikUangActivity
import id.app.wawetech.massagemitra.RiwayatTransaksiActivity


class Saldo : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.saldo_frag, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<RelativeLayout>(R.id.btnSetorUang).setOnClickListener {
            startActivity(Intent(activity,SetorUangActivity::class.java))
        }

        view.findViewById<RelativeLayout>(R.id.btnTarikUang).setOnClickListener {
            startActivity(Intent(activity, TarikUangActivity::class.java))
        }

        view.findViewById<RelativeLayout>(R.id.btnRiwayatTransaksi).setOnClickListener {
            startActivity(Intent(activity, RiwayatTransaksiActivity::class.java))
        }
    }
}