package id.app.wawetech.massagemitra.fragment

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.*
import id.app.wawetech.massagemitra.adapter.ViewPagerAdapter
import id.app.wawetech.massagemitra.fragment.fragment_pesanan.*
import id.app.wawetech.massagemitra.R

class Pesanan : Fragment() {


    var viewpager: ViewPager? = null
    var tabs: TabLayout? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.pesanan_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewpager = view.findViewById(R.id.container)
        tabs = view.findViewById(R.id.tabs)
        setupViewpager(this.viewpager!!)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.tabs_nav, menu)
    }

    fun setupViewpager(viewPager: ViewPager) {
        Log.e("", "setup viewpager")
        val adapter = ViewPagerAdapter(childFragmentManager)
        adapter.addFragment(AktifFrag())
        adapter.addFragment(SelesaiFrag())
        viewpager?.adapter = adapter
        tabs?.setupWithViewPager(viewPager)
        tabs?.getTabAt(0)!!.text = "Aktif"
        tabs?.getTabAt(1)!!.text = "Selesai"
    }
}