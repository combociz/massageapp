package id.app.wawetech.massagemitra.adapter

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import id.app.wawetech.massagemitra.R
import java.text.FieldPosition

/**
 * TODO HANYA UNTUK DEMO SILAKAN BUAT SENDIRI ADAPTERNYA
 */

class DummyAdapter(var dummydata: List<Int>,var itemlistener : ItemListener) : RecyclerView.Adapter<DummyAdapter.DummyHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): DummyHolder {
        return DummyHolder(
            LayoutInflater.from(p0.context).inflate(
                R.layout.row_pesanan_aktif,
                p0,
                false
            ) as CardView
        ,itemlistener)
    }

    override fun getItemCount(): Int {
        return dummydata.size
    }

    override fun onBindViewHolder(p0: DummyHolder, p1: Int) {
        p0.bindData(dummydata[p1])
    }


    interface ItemListener{
        fun selectItem(position: Int)
    }

    inner class DummyHolder(view: View,var listener: ItemListener) : RecyclerView.ViewHolder(view) {

        var btnAbaikan = view.findViewById<Button>(R.id.btnAbaikan)
        var btnAmbil = view.findViewById<Button>(R.id.btnAmbil)

        fun bindData(any : Any){
            btnAmbil.setOnClickListener {
                listener.selectItem(adapterPosition)
            }

            btnAbaikan.setOnClickListener {
                listener.selectItem(adapterPosition)
            }
        }

    }
}