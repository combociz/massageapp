package id.app.wawetech.massagemitra.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.app.wawetech.massagemitra.R

class SelesaiAdapter(var list: MutableList<Any>, var listener: ItemListener) :
    RecyclerView.Adapter<SelesaiAdapter.Holder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Holder {
        return Holder(LayoutInflater.from(p0.context).inflate(R.layout.row_pesanan_selesai, p0, false), listener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: Holder, p1: Int) {
        p0.bindData()
    }

    interface ItemListener {
        fun openData()
    }

    class Holder(var view: View, var listener: ItemListener) : RecyclerView.ViewHolder(view) {

        fun bindData() {
            view.setOnClickListener {
                listener.openData()
            }
        }

    }

}