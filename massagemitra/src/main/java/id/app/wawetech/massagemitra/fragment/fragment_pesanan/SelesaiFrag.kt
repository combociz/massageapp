package id.app.wawetech.massagemitra.fragment.fragment_pesanan

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.app.wawetech.massagemitra.R
import id.app.wawetech.massagemitra.adapter.SelesaiAdapter
import id.app.wawetech.massagemitra.*


class SelesaiFrag : Fragment() {


    lateinit var list: RecyclerView

    fun generateData(): MutableList<Any> {
        var datas: MutableList<Any> = ArrayList()
        for (i in 0 until 10) {
            datas.add(i)
        }
        return datas
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.selesai_frag, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list = view.findViewById(R.id.listSelesai)
        list.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        list.adapter = SelesaiAdapter(generateData(), object : SelesaiAdapter.ItemListener {
            override fun openData() {
                startActivity(Intent(activity,DetailPesananActivity::class.java))
            }
        })
    }

}