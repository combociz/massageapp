package id.app.wawetech.massagemitra.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import id.app.wawetech.massagemitra.R
import id.app.wawetech.massagemitra.UlasanActivity

class Statistik : Fragment() {

    lateinit var btnShowUlasan: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.statistik_frag, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnShowUlasan = view.findViewById(R.id.btnShowUlasan)
        btnShowUlasan.setOnClickListener {
            startActivity(Intent(context, UlasanActivity::class.java))
        }
    }
}