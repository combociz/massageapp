package id.app.wawetech.massagemitra.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.app.wawetech.massagemitra.R
import id.app.wawetech.massagemitra.adapter.DummyAdapter
import android.support.v7.widget.PagerSnapHelper
import android.support.v7.widget.SnapHelper
import android.widget.CompoundButton
import android.widget.LinearLayout
import android.widget.Switch
import android.widget.Toast
import kotlinx.android.synthetic.main.beranda_frag.*


class Beranda : Fragment() {

    lateinit var list: RecyclerView
    var listdummy: MutableList<Int> = ArrayList()
    lateinit var adapter: DummyAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.beranda_frag, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        list = view.findViewById(R.id.listpesanan)
        var lineTop = view.findViewById<LinearLayout>(R.id.lineTop)
        var switch = view.findViewById<Switch>(R.id.switcher)
        switch.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked) {
                    lineTop.visibility = View.GONE
                    list.visibility = View.VISIBLE
                    if(listdummy.isEmpty()){
                        setDummyAdapter()
                        adapter.notifyDataSetChanged()
                    }
                }
            }
        })
        val snapHelper = PagerSnapHelper()
        setDummyAdapter()
        adapter = DummyAdapter(listdummy, object : DummyAdapter.ItemListener {
            override fun selectItem(position: Int) {
                listdummy.removeAt(position)
                adapter.notifyItemRemoved(position)
                Toast.makeText(context,listdummy.size.toString(),Toast.LENGTH_SHORT).show()
                if(listdummy.isEmpty()){
                    switcher.isChecked = false
                    lineTop.visibility = View.VISIBLE
                    list.visibility = View.GONE
                }
            }
        })
        list.adapter = adapter
        snapHelper.attachToRecyclerView(list)
        list.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
    }

    fun setDummyAdapter() {
        for (i in 1 until 5) {
            listdummy.add(i)
        }
    }
}