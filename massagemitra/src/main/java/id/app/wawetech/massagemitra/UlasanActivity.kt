package id.app.wawetech.massagemitra

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import id.app.wawetech.massagemitra.adapter.UlasanAdapter

class UlasanActivity : AppCompatActivity() {


    lateinit var list: RecyclerView

    fun generateData(): MutableList<Any> {
        var datas: MutableList<Any> = ArrayList()
        for (i in 0 until 10) {
            datas.add(i)
        }
        return datas
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ulasan_activity)
        list = findViewById(R.id.list)
        title = "Ulasan"
        list.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
        list.adapter = UlasanAdapter(generateData())

    }
}