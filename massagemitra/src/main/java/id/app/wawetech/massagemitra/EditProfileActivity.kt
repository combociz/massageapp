package id.app.wawetech.massagemitra

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class EditProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_akun_activity)
        title = "Akun"
    }
}