package id.app.wawetech.massagemitra.adapter

import android.annotation.SuppressLint
import android.graphics.drawable.ColorDrawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.util.zip.Inflater
import id.app.wawetech.massagemitra.R


class RiwayatAdapter(var list: MutableList<String>) : RecyclerView.Adapter<RiwayatAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.row_transaksi, p0, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindData(list[p1])
    }


    class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        var info2 = view.findViewById<TextView>(R.id.info2)
        var lineVer = view.findViewById<View>(R.id.verline)
        var lineVer2 = view.findViewById<View>(R.id.verline2)
        var info4 = view.findViewById<TextView>(R.id.info4)

        fun bindData(string: String) {
            if (string == "Pengurangan") {
                info2.text = "Pengurangan"
                info4.text = "Penarikan Uang"
                lineVer.visibility = View.VISIBLE
                lineVer2.visibility = View.GONE
            } else {
                info2.text = "Penambahan"
                info4.text = "Pembayaran Jasa Pijat"
                lineVer.visibility = View.GONE
                lineVer2.visibility = View.VISIBLE
            }
        }
    }

}