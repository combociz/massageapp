package id.app.wawetech.massagemitra

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.TextView

class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        title = "Akun"
        findViewById<ImageView>(R.id.btnEdit).setOnClickListener {
            startActivity(Intent(this@ProfileActivity,EditProfileActivity::class.java))
        }
        findViewById<TextView>(R.id.btnBantuan).setOnClickListener {
            startActivity(Intent(this@ProfileActivity,BantuanActivity::class.java))
        }
    }
}