package id.app.wawetech.massagecustomer.fragment_home.fragment_pesanan

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.app.wawetech.massagecustomer.R
import id.app.wawetech.massagecustomer.pojo.Pesanan
import id.app.wawetech.massagecustomer.pojo.*
import id.app.wawetech.massagecustomer.*
import id.app.wawetech.massagecustomer.adapter.*

class ProsesFrag : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.task_belum_selesai_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val rc = view.findViewById<RecyclerView>(R.id.list_pesanan)
        val obj = object : PesananAdapter.ItemListener {
            override fun onClick(pesanan: Pesanan) {
                if (pesanan.status.equals("Pesanan selesai")) {
                    //finish work
                    startActivity(Intent(activity, Ulasan::class.java))
                }else{
                    //ongoing work
                    startActivity(Intent(activity, DetailPesananPremi::class.java))
                }
            }
        }
        rc.adapter = PesananAdapter(listorder,obj)
        rc.layoutManager = LinearLayoutManager(activity)
    }

    val listorder: ArrayList<Pesanan>
        get() {
            val result = ArrayList<Pesanan>()
            result.add(
                Pesanan(
                    "Pijat Tradisional",
                    "Rp.100.000",
                    "01:00 PM, 18 Nov 2018",
                    "Pesanan diterima"
                )
            )
            result.add(
                Pesanan(
                    "Pijat Terapi China",
                    "Rp.100.000",
                    "01:00 PM, 18 Nov 2018",
                    "Pesanan diterima"
                )
            )
            result.add(
                Pesanan(
                    "Pijat Pegal-Pegal",
                    "Rp.100.000",
                    "01:00 PM, 18 Nov 2018",
                    "Pesanan diterima"
                )
            )
            result.add(
                Pesanan(
                    "Pijat Reflexi",
                    "Rp.100.000",
                    "01:00 PM, 18 Nov 2018",
                    "Pesanan diterima"
                )
            )
            result.add(
                Pesanan(
                    "Pijat Full Body",
                    "Rp.100.000",
                    "01:00 PM, 18 Nov 2018",
                    "Pesanan diterima"
                )
            )
            result.add(
                Pesanan(
                    "Pijat Kaki",
                    "Rp.100.000",
                    "01:00 PM, 18 Nov 2018",
                    "Pesanan diterima"
                )
            )
            return result
        }
}