package id.app.wawetech.massagecustomer

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import id.app.wawetech.massagecustomer.adapter.FavoritAdapter
import id.app.wawetech.massagecustomer.pojo.Favorit

class JasaFavorit : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.penyedia_jasa_favorit)
        title = "Penyedia Favorit"

        var adapter: FavoritAdapter = FavoritAdapter(getListTerapis())
        var rc = findViewById<RecyclerView>(R.id.list_favorit)
        rc.layoutManager = GridLayoutManager(applicationContext, 2)
        rc.adapter = adapter
    }

    fun getListTerapis(): ArrayList<Favorit> {
        val result = ArrayList<Favorit>()
        for (i in 1..5) {
            result.add(
                Favorit(
                    "Karyawan Pijat $i",
                    "+62783217$i",
                    "jalan jakarta no $i"
                )
            )
        }
        return result
    }
}