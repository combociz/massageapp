package id.app.wawetech.massagecustomer

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.TextView

class Ulasan : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ulasan_activity)
        title = "Ulasan"
        var lapor = findViewById(R.id.btnReport) as TextView
        lapor.setOnClickListener {
            startActivity(Intent(applicationContext,Laporkan::class.java))
        }

    }
}