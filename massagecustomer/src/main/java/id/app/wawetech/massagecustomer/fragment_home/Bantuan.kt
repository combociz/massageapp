package id.app.wawetech.massagecustomer.fragment_home

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_detail_pesanan.*
import id.app.wawetech.massagecustomer.R;
import id.app.wawetech.massagecustomer.*

public class Bantuan : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.bantuan_fragment, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.findViewById<TextView>(R.id.txtHubungiKami)
            .setOnClickListener {
                startActivity(Intent(activity, Laporkan::class.java))
            }
        view.findViewById<TextView>(R.id.txtKeluhan)
            .setOnClickListener {
                startActivity(Intent(activity, Keluhan::class.java))
            }
    }
}