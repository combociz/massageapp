package id.app.wawetech.massagecustomer

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import id.app.wawetech.massagecustomer.adapter.TherapyAdapter
import id.app.wawetech.massagecustomer.adapter.TherapyAdapter.ItemListener
import id.app.wawetech.massagecustomer.pojo.ModelTherapy

class TherapiActivity : AppCompatActivity() {
    lateinit var rc: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.therapy_activity)
        var _intent: Intent = intent
        var terapi = _intent.getStringExtra("terapi")
        title = "$terapi Spa"
        rc = findViewById(R.id.listTherapi)

        val obj = object : ItemListener{
            override fun onItemClick(terapi: ModelTherapy) {
                startActivity(Intent(applicationContext, DetailPesanan::class.java))
            }
        }

        rc.layoutManager = LinearLayoutManager(applicationContext)
        rc.adapter = TherapyAdapter(
            getListTerapi(terapi),
            obj
        )
    }


    fun getListTerapi(terapi: String): ArrayList<ModelTherapy> {
        val result = ArrayList<ModelTherapy>()
        for (i in 1..5) {
            result.add(
                ModelTherapy(
                    "Pijat $terapi Tradisional $i",
                    "Lorem ipsum dolor sit amet consectetur adipiscing elit",
                    getImage(terapi)
                )
            )
        }
        return result
    }

    fun getImage(terapi: String): Int {
        when (terapi) {
            "face" -> return R.drawable.face_spa_image
            "body" -> return R.drawable.body_spa_image
            "reflex" -> return R.drawable.foot_reflexologi
        }
        return 0
    }
}