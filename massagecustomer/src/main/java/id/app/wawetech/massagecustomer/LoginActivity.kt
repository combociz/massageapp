package id.app.wawetech.massagecustomer

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import java.util.concurrent.TimeUnit
import com.google.android.gms.tasks.TaskExecutors
import com.google.firebase.auth.PhoneAuthProvider


public class LoginActivity : AppCompatActivity() {

    val TAG = "logger"
    var storedVerificationId: String = ""
    var resendToken: PhoneAuthProvider.ForceResendingToken? = null
    var mAuth: FirebaseAuth? = null


    var callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        override fun onVerificationCompleted(credential: PhoneAuthCredential) {
            Log.e(TAG, "verify complete")
        }

        override fun onVerificationFailed(e: FirebaseException) {
            Log.e(TAG, "onVerificationFailed", e)
            if (e is FirebaseAuthInvalidCredentialsException) {
                Toast.makeText(applicationContext, "Invalid request", Toast.LENGTH_LONG).show()
            } else if (e is FirebaseTooManyRequestsException) {
                Toast.makeText(applicationContext, "SMS quota for project has been reached", Toast.LENGTH_LONG)
                    .show()
            }
        }

        override fun onCodeSent(verificationId: String?, token: PhoneAuthProvider.ForceResendingToken) {
            Log.e(TAG, "onCodeSent:" + verificationId!!)
            storedVerificationId = verificationId
            resendToken = token
            Log.e(TAG, "onVerificationCompleted: ${token}")
            val code: String = verificationId
            val intent: Intent = Intent(applicationContext, VerificationAuth::class.java)
            intent.putExtra("code", code)
            startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)
        FirebaseApp.initializeApp(this)

        var edtPhoneNumber: EditText = findViewById(R.id.edtNumber)
        findViewById<Button>(R.id.btnMasuk).setOnClickListener {
            Log.e(TAG, "clicked")
//            startActivity(Intent(this@LoginActivity,HomeActivity::class.java))
            sendVerificationCode(edtPhoneNumber.text.toString())
        }
        findViewById<TextView>(R.id.txtDaftardisini).setOnClickListener {
            startActivity(Intent(this, RegistryActivity::class.java))
        }
    }


    private fun sendVerificationCode(mobile: String) {
        Log.e(TAG, mobile)
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            "+62$mobile",
            60,
            TimeUnit.SECONDS,
            TaskExecutors.MAIN_THREAD,
            callbacks
        )
    }
}