package id.app.wawetech.massagecustomer

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.*
import kotlinx.android.synthetic.main.cek_lokasi_activity.*
import kotlinx.android.synthetic.main.konfirmasi_pesanan_activity.*
import kotlinx.android.synthetic.main.konfirmasi_pesanan_activity.view.*
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.min

class Konfirmasi_Pesanan : AppCompatActivity() {


    val REQUEST_MAP = 64
    lateinit var clock: TextView
    lateinit var calendar: TextView
    lateinit var location: TextView
    lateinit var spinner: Spinner
    var jenkel = "Pria"
    var penyedia = "Pria"
    var price = "0"
    var durasi = "60 menit"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.konfirmasi_pesanan_activity)
        title = "Konfirmasi Pesanan"
        clock = findViewById(R.id.clock)
        calendar = findViewById(R.id.date)
        location = findViewById(R.id.Choose)
        spinner = findViewById(R.id.spinPreferensi)


        var intent = intent
        jenkel = intent.getStringExtra("jenkel")
        penyedia = intent.getStringExtra("penyedia")
        price = intent.getStringExtra("price")
        durasi = intent.getStringExtra("durasi")

        findViewById<TextView>(R.id.txtMenit).text = durasi
        findViewById<TextView>(R.id.txtPrice).text = price
        var radioGopay = findViewById<RadioButton>(R.id.radioGopay)
        var radioTcash = findViewById<RadioButton>(R.id.radioTcash)
        var radioMandiri = findViewById<RadioButton>(R.id.radioMandiri)
        var radioTunai = findViewById<RadioButton>(R.id.radioTunai)

        radioGopay.setOnCheckedChangeListener { buttonView, isChecked ->
            radioMandiri.isChecked = false
            radioTcash.isChecked = false
            radioTunai.isChecked = false
        }
        radioTcash.setOnCheckedChangeListener { buttonView, isChecked ->
            radioGopay.isChecked = false
            radioMandiri.isChecked = false
            radioTunai.isChecked = false
        }
        radioMandiri.setOnCheckedChangeListener { buttonView, isChecked ->
            radioGopay.isChecked = false
            radioTcash.isChecked = false
            radioTunai.isChecked = false
        }
        radioTunai.setOnCheckedChangeListener { buttonView, isChecked ->
            radioGopay.isChecked = false
            radioTcash.isChecked = false
            radioMandiri.isChecked = false
        }

        val listItemsTxt = arrayOf("Pria", "Wanita")
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, listItemsTxt)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = aa
        location.setOnClickListener {
            startActivityForResult(Intent(applicationContext, MapsActivity::class.java), REQUEST_MAP)
        }


        findViewById<Button>(R.id.btnPesan)
            .setOnClickListener {
                startActivity(Intent(applicationContext, DetailPesanan::class.java))
            }

        clock.setOnClickListener {
            var cal = Calendar.getInstance()
            var tmt = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hourOfDay)
                cal.set(Calendar.MINUTE, minute)
                var str: StringBuilder = StringBuilder(getReal(hourOfDay))
                    .append(":")
                    .append(getReal(minute))
                clock.text = str.toString()
            }, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true)
            tmt.show()
        }

        calendar.setOnClickListener {
            var cal = Calendar.getInstance()
            DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in textbox
                    cal.set(Calendar.YEAR, year)
                    cal.set(Calendar.YEAR, monthOfYear)
                    cal.set(Calendar.YEAR, year)
                    calendar.text = SimpleDateFormat("EEEE, MMM yyyy").format(cal.time)
                },
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_MAP) {
            if (resultCode == Activity.RESULT_OK) {
                var alamat = data?.getStringExtra("alamat")
                location.text = alamat
            }
        }
    }

    fun getReal(obj: Int): String {
        return if (obj < 10) "0$obj" else obj.toString()
    }
}