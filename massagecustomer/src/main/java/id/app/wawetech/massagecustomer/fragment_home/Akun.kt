package id.app.wawetech.massagecustomer.fragment_home

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import id.app.wawetech.massagecustomer.R
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import id.app.wawetech.massagecustomer.*

public class Akun : Fragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bg -> ubahProfil()
            R.id.bg2 -> ubahProfil()
            R.id.btnPenyediaJasaFavorit -> startActivity(Intent(activity,JasaFavorit::class.java))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.akun_activity, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.findViewById<ImageView>(R.id.bg).setOnClickListener(this)
        view.findViewById<ImageView>(R.id.bg2).setOnClickListener(this)
        view.findViewById<TextView>(R.id.btnPenyediaJasaFavorit).setOnClickListener(this)
    }

    fun ubahProfil() {
        startActivity(Intent(activity, AkunEdit::class.java))
    }
}