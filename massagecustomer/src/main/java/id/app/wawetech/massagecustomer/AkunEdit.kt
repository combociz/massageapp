package id.app.wawetech.massagecustomer

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class AkunEdit : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_akun_activity)
        title = "Edit Akun"
    }
}