package id.app.wawetech.massagecustomer

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button

class WarningLocation : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.warning_lokasi_fragment)
        title = "Aktifkan Lokasi"
        findViewById<Button>(R.id.btnaktifkan).setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
        }
    }
}