package id.app.wawetech.massagecustomer

import android.annotation.SuppressLint
import android.location.Geocoder
import android.support.v4.app.FragmentActivity
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import android.os.Bundle
import android.util.Log
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.LatLng
import java.util.*
import android.location.Address;
import android.widget.Button
import android.widget.TextView
import java.nio.file.Files.size
import android.content.Intent
import android.widget.Toast


class MapsActivity : FragmentActivity(), OnMapReadyCallback {


    private var mMap: GoogleMap? = null
    private var TAG = "Maps"
    private val REQUEST_MAP = 64
    lateinit var geocoder: Geocoder
    var yourAddresses: MutableList<Address>? = null
    lateinit var locationaddress: TextView
    lateinit var btnOke : Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        geocoder = Geocoder(applicationContext, Locale("ID"))
        locationaddress = findViewById(R.id.address)
        btnOke = findViewById(R.id.btnOke)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        btnOke.setOnClickListener {
            if(!locationaddress.text.isEmpty()){
                // put the String to pass back into an Intent and close this activity
                val intent = Intent()
                intent.putExtra("alamat", locationaddress.text)
                setResult(RESULT_OK, intent)
                finish()
            }
        }
    }


    @SuppressLint("SetTextI18n")
    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0

        mMap?.setOnCameraMoveStartedListener {
            Log.e(TAG, "move $it")
        }

        mMap?.setOnCameraIdleListener {
            if (mMap != null) {
                mMap?.clear()
            }
            val centerLatLang = mMap?.projection?.visibleRegion?.latLngBounds?.center
//            mMap?.addMarker(MarkerOptions().position(centerLatLang!!).title("Your Location Here"))
            Log.e(TAG, "idle $centerLatLang")
            yourAddresses = geocoder?.getFromLocation(centerLatLang!!.latitude, centerLatLang!!.longitude, 1)
            if (yourAddresses?.size != 0) {
                val yourAddress =
                    if (yourAddresses!!.get(0).getAddressLine(0) != null) yourAddresses!!.get(0).getAddressLine(0) else ""
                val yourCity =
                    if (yourAddresses!!.get(0).getAddressLine(1) != null) yourAddresses!!.get(0).getAddressLine(1) else ""
                val yourCountry =
                    if (yourAddresses!!.get(0).getAddressLine(2) != null) yourAddresses!!.get(0).getAddressLine(2) else ""
                Log.e(TAG, "$yourAddress,$yourCity,$yourCountry")
                locationaddress.text = "$yourAddress $yourCity $yourCountry"
                if(!locationaddress.text.isEmpty()){
                    btnOke.isEnabled = true
                }else{
                    btnOke.isEnabled = false
                    Toast.makeText(applicationContext,"Alamat tidak valid", Toast.LENGTH_LONG)
                }
            }

//            mMap?.moveCamera(CameraUpdateFactory.newLatLng(centerLatLang))
        }
//        val centerLatLang = mMap?.getProjection()?.visibleRegion?.latLngBounds?.center
//        val sydney = LatLng(centerLatLang!!.latitude, centerLatLang!!.longitude)
    }
}