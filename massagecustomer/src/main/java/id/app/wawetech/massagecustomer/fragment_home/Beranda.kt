package id.app.wawetech.massagecustomer.fragment_home

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import id.app.wawetech.massagecustomer.R;
import ss.com.bannerslider.ImageLoadingService
import ss.com.bannerslider.Slider
import id.app.wawetech.massagecustomer.utils.PicassoImageLoadingService
import ss.com.bannerslider.Slider.init
import id.app.wawetech.massagecustomer.adapter.MainSliderAdapter
import id.app.wawetech.massagecustomer.*

public class Beranda : Fragment() {
    var slider: Slider? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.beranda_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        slider = view.findViewById<Slider>(R.id.banner_slider)
        init(PicassoImageLoadingService(activity!!))
        val listimage: MutableList<String> = ArrayList<String>()
        listimage.add("https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.estudiospatrimoniales.org%2Fwp-content%2Fuploads%2F2017%2F02%2Fhome-22-banner-02.jpg&f=1")
        listimage.add("https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fdiscount-coupon-codes.upto75.com%2Fuploadimages%2Fsale_mainpic_20081208082555KeralaAyurvedaClinic-banner.jpg&f=1")
        listimage.add("https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fdiscount-coupon-codes.upto75.com%2Fuploadimages%2Fsales_offer_mainpic_20101222150206VivaCity_Banner1.png&f=1")
        slider?.setAdapter(MainSliderAdapter(listimage))

        val intent = Intent(activity, TherapiActivity::class.java)

        view.findViewById<Button>(R.id.btnBody)
            .setOnClickListener {
                intent.putExtra("terapi", "body")
                startActivity(intent)
            }

        view.findViewById<Button>(R.id.btnFace)
            .setOnClickListener {
                intent.putExtra("terapi", "face")
                startActivity(intent)
            }

        view.findViewById<Button>(R.id.btnReflex)
            .setOnClickListener {
                intent.putExtra("terapi", "reflex")
                startActivity(intent)
            }

    }


}