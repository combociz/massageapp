package id.app.wawetech.massagecustomer

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider

class VerificationAuth: AppCompatActivity() {

    var mAuth : FirebaseAuth? = null
    var verificationId : String = ""
    lateinit var credential: PhoneAuthCredential
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.konfirmasi_otp_activity)
        //initializing objects
        mAuth = FirebaseAuth.getInstance()
        val intent = intent
        verificationId = intent.getStringExtra("code")
        val edtOTP = findViewById<EditText>(R.id.edtOTP)
        val btn : Button = findViewById<Button>(R.id.btnKonfirmasi)
        btn.setOnClickListener {
            Log.e("", verificationId)
            Log.e("", edtOTP.text.toString())
            verifyVerificationCode(verificationId,edtOTP.text.toString())
        }
    }


    private fun verifyVerificationCode(verificationID : String,otp: String) {
        //creating the credential
        Log.e("request1",verificationID)
        Log.e("request2",otp)
        credential = PhoneAuthProvider.getCredential(verificationID, otp)
        //signing the user
        signInWithPhoneAuthCredential(this.credential)
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        mAuth?.signInWithCredential(credential)?.addOnCompleteListener(this, {
            Log.e("result",""+it.isSuccessful)
            if (it.isSuccessful) {
                startActivity(Intent(this, WarningLocation::class.java))
            } else {
                Log.e("VerificationAuth", "login error")
            }
        })
    }
}