package id.app.wawetech.massagecustomer

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView

class DetailPesanan : AppCompatActivity() {


    var jenkel: String = "Pria"
    var penyedia: String = "Pria"
    var durasi: String = "60 menit"
    var price: String = "0"

    @SuppressLint("WrongViewCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_pesanan)
        title = "Detail Pesanan"
        var textViewTotal = findViewById<TextView>(R.id.txtTotalPrice)
        var btnLanjut = findViewById<TextView>(R.id.btnLanjut)
        btnLanjut.setOnClickListener {
            startActivity(
                Intent(applicationContext, Konfirmasi_Pesanan::class.java)
                    .putExtra("jenkel", jenkel)
                    .putExtra("penyedia", penyedia)
                    .putExtra("price", price)
                    .putExtra("durasi", durasi)
            )
        }
        var btnPria = findViewById<TextView>(R.id.btnPria)
        var btnWanita = findViewById<TextView>(R.id.btnWanita)

        btnPria.setOnClickListener {
            btnPria.setBackgroundResource(R.drawable.rounded_gray)
            btnWanita.setBackgroundResource(R.drawable.rounded_violet)
            jenkel = "Pria"
        }

        btnWanita.setOnClickListener {
            btnPria.setBackgroundResource(R.drawable.rounded_violet)
            btnWanita.setBackgroundResource(R.drawable.rounded_gray)
            jenkel = "Wanita"
        }

        var btn60 = findViewById<Button>(R.id.btn60)
        var btn90 = findViewById<Button>(R.id.btn90)
        var btn120 = findViewById<Button>(R.id.btn120)


        btn60.setOnClickListener {
            btn60.setBackgroundResource(R.drawable.circlebtn_white)
            btn90.setBackgroundResource(R.drawable.circlebtn_violet)
            btn120.setBackgroundResource(R.drawable.circlebtn_violet)
            textViewTotal.setText("Rp. 70.000")
            price = "70.000"
        }

        btn90.setOnClickListener {
            btn90.setBackgroundResource(R.drawable.circlebtn_white)
            btn60.setBackgroundResource(R.drawable.circlebtn_violet)
            btn120.setBackgroundResource(R.drawable.circlebtn_violet)
            textViewTotal.setText("Rp. 110.000")
            price = "110.000"
        }

        btn120.setOnClickListener {
            btn120.setBackgroundResource(R.drawable.circlebtn_white)
            btn90.setBackgroundResource(R.drawable.circlebtn_violet)
            btn60.setBackgroundResource(R.drawable.circlebtn_violet)
            textViewTotal.setText("Rp. 150.000")
            price = "150.000"
        }

        var txtPria = findViewById<TextView>(R.id.txtJasaPria)
        var txtWanita = findViewById<TextView>(R.id.txtJasaWanita)


        txtPria.setOnClickListener {
            txtPria.setBackgroundResource(R.drawable.rounded_gray)
            txtWanita.setBackgroundResource(R.drawable.rounded_violet)
            penyedia = "Pria"
        }

        txtWanita.setOnClickListener {
            txtWanita.setBackgroundResource(R.drawable.rounded_gray)
            txtPria.setBackgroundResource(R.drawable.rounded_violet)
            penyedia = "Wanita"
        }

    }
}