package id.app.wawetech.massagecustomer.fragment_home.fragment_pesanan

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.app.wawetech.massagecustomer.R
import id.app.wawetech.massagecustomer.pojo.Pesanan
import id.app.wawetech.massagecustomer.adapter.*
import id.app.wawetech.massagecustomer.*
import id.app.wawetech.massagecustomer.pojo.ModelTherapy
import java.util.*

class SelesaiFrag : Fragment() {

//    import id.app.wawetech.massagecustomer.pojo.*;

    val obj = object : PesananAdapter.ItemListener {
        override fun onClick(pesanan: Pesanan) {
            if (pesanan.status.equals("Pesanan selesai")) {
                startActivity(Intent(activity, Ulasan::class.java))
            } else {
                startActivity(Intent(activity, Ulasan::class.java))
            }
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.task_sudah_selesai_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.e("data list ", "length" + listorder.size)
        val rc = view.findViewById<RecyclerView>(R.id.list_pesanan)
        val obj = object : PesananAdapter.ItemListener {
            override fun onClick(pesanan: Pesanan) {
                if (pesanan.status.equals("Pesanan selesai")) {
                    //finish work
                    startActivity(Intent(activity, Ulasan::class.java))
                }else{
                    //ongoing work
                    startActivity(Intent(activity, DetailPesananPremi::class.java))
                }
            }
        }
        rc.adapter = PesananAdapter(listorder, obj)
        rc.layoutManager = LinearLayoutManager(activity)
    }

    val listorder: ArrayList<Pesanan>
        get() {
            val result = ArrayList<Pesanan>()
            result.add(
                Pesanan("Pijat Tradisional", "Rp.100.000", "01:00 PM, 18 Nov 2018", "Pesanan selesai")
            )
            result.add(
                Pesanan("Pijat Terapi China", "Rp.100.000", "01:00 PM, 18 Nov 2018", "Pesanan selesai")
            )
            result.add(
                Pesanan("Pijat Pegal-Pegal", "Rp.100.000", "01:00 PM, 18 Nov 2018", "Pesanan selesai")
            )
            result.add(
                Pesanan("Pijat Reflexi", "Rp.100.000", "01:00 PM, 18 Nov 2018", "Pesanan selesai")
            )
            result.add(
                Pesanan("Pijat Full Body", "Rp.100.000", "01:00 PM, 18 Nov 2018", "Pesanan selesai")
            )
            result.add(
                Pesanan("Pijat Kaki", "Rp.100.000", "01:00 PM, 18 Nov 2018", "Pesanan selesai")
            )
            return result
        }

}