package id.app.wawetech.massagecustomer.adapter

import ss.com.bannerslider.adapters.SliderAdapter
import ss.com.bannerslider.viewholder.ImageSlideViewHolder

class MainSliderAdapter internal constructor(var list : List<String>): SliderAdapter() {

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindImageSlide(position: Int, imageSlideViewHolder: ImageSlideViewHolder?) {
            imageSlideViewHolder?.bindImageSlide(list.get(position))
    }
}