package id.app.wawetech.massagecustomer.adapter

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import id.app.wawetech.massagecustomer.R
import id.app.wawetech.massagecustomer.pojo.Pesanan

class PesananAdapter(private val pesanan : ArrayList<Pesanan>,var listener : ItemListener) : RecyclerView.Adapter<PesananAdapter.PesananHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PesananHolder {
        return PesananHolder(
            LayoutInflater.from(p0.context)
                .inflate(R.layout.row_pesanan, p0, false) as LinearLayout
        ,listener)
    }

    override fun getItemCount(): Int {
        return pesanan.toArray().count()
    }

    override fun onBindViewHolder(p0: PesananHolder, p1: Int) {
        Log.e("binder","binddata"+p0+":"+pesanan[p1])
        p0.UpdateWithOrder(pesanan[p1])

    }

    interface ItemListener{
        fun onClick(pesanan: Pesanan)
    }

    class PesananHolder(itemview: View,listener: ItemListener) : RecyclerView.ViewHolder(itemview) {

        private var txtNama: TextView = itemview.findViewById(R.id.txtLayanan)
        private var txtDate: TextView = itemview.findViewById(R.id.txtDate)
        private var txtPrice: TextView = itemview.findViewById(R.id.txtPrice)
        private var btnStatusPesanan : TextView = itemview.findViewById(R.id.btnPesanditerima)
        private var listener : ItemListener = listener

        fun UpdateWithOrder(pesanan: Pesanan) {
            txtNama.text = pesanan.nama
            txtDate.text = pesanan.jam
            txtPrice.text = pesanan.harga
            btnStatusPesanan.text = pesanan.status
            if(pesanan.status.equals("Pesanan selesai")){
                btnStatusPesanan.setTextColor(Color.parseColor("#2E7D32"))
            }else{
                btnStatusPesanan.setTextColor(Color.parseColor("#c62828"))
            }
            itemView.setOnClickListener { listener.onClick(pesanan) }

            fun String.toColor(): Int = Color.parseColor(this)
        }

    }
}