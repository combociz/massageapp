package id.app.wawetech.massagecustomer.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import id.app.wawetech.massagecustomer.R
import id.app.wawetech.massagecustomer.pojo.Favorit
import id.app.wawetech.massagecustomer.pojo.Pesanan

class FavoritAdapter(private val listfavorit : ArrayList<Favorit>) : RecyclerView.Adapter<FavoritAdapter.FavoritHolder>()  {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): FavoritHolder {
        return FavoritHolder(
            LayoutInflater.from(p0.context)
                .inflate(R.layout.row_penyedia_jasa_favorit, p0, false) as RelativeLayout
        )
    }

    override fun getItemCount(): Int {
        return listfavorit.size
    }

    override fun onBindViewHolder(p0: FavoritHolder, p1: Int) {
        p0.UpdateData(listfavorit[p1])
    }


    class FavoritHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

        var name = itemView.findViewById<TextView>(R.id.txtName)
        var notelp = itemView.findViewById<TextView>(R.id.txtPhone)
        var alamat = itemView.findViewById<TextView>(R.id.txtLocation)

        fun UpdateData(favorit : Favorit){
            name.text = favorit.nama
            notelp.text = favorit.notelp
            alamat.text = favorit.alamat
        }
    }
}