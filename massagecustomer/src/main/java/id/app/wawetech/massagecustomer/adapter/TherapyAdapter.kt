package id.app.wawetech.massagecustomer.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.squareup.picasso.Picasso
import id.app.wawetech.massagecustomer.R
import id.app.wawetech.massagecustomer.pojo.ModelTherapy

public class TherapyAdapter(var adapter: ArrayList<ModelTherapy>, var listener: ItemListener) :
    RecyclerView.Adapter<TherapyAdapter.TherapyHolder>() {

    public var _mylistener: ItemListener = listener

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TherapyHolder {
        return TherapyHolder(
            LayoutInflater.from(p0.context)
                .inflate(R.layout.row_face_spa_detail, p0, false) as RelativeLayout
            , _mylistener
        )
    }

    override fun getItemCount(): Int {
        return adapter.size
    }

    override fun onBindViewHolder(p0: TherapyHolder, p1: Int) {
        p0.UpdateWithOrder(adapter.get(p1))
    }

    interface ItemListener{
        fun onItemClick(terapi: ModelTherapy)
    }


    class TherapyHolder(itemview: View, listener: ItemListener) : RecyclerView.ViewHolder(itemview) {
        private var image_ter: ImageView = itemview.findViewById(R.id.image_therapy)
        private var namater: TextView = itemview.findViewById(R.id.title)
        private var keteranganter: TextView = itemview.findViewById(R.id.keterangan)
        private var listener: ItemListener = listener


        fun UpdateWithOrder(terapi: ModelTherapy) {
            namater.text = terapi.name
            Picasso.with(itemView.context)
                .load(terapi.img)
                .noFade()
                .into(image_ter)
            keteranganter.text = terapi.keterangan
            image_ter.setOnClickListener {
                listener.onItemClick(terapi)
            }
        }
    }

}